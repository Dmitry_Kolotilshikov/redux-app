import { NgModule } from "@angular/core";
import { RouterModule, Route } from "@angular/router";

const routes: Route[] = [
    {
        path: '',
        redirectTo: 'list',
        pathMatch: 'full'
    },
    {
        path: 'list',
        loadChildren: ()=> import('./list/list.module').then(m=>m.ListModule)
    },
    {
        path: 'edit',
        loadChildren: ()=> import('./edit/edit.module').then(m=>m.EditModule)
    }
]

@NgModule({
    imports: [
        RouterModule.forRoot(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class AppRoutingModule {}