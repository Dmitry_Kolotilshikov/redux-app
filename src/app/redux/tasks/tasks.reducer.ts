import { Task } from 'src/app/models/task';
import { TasksAction, TASKS_ACTIONS } from './tasks.action';

export interface State {
    tasks: Task[];
}

const initialState: State = {
    tasks: []
}

export function tasksReducer(state = initialState, action: TasksAction) {
    switch (action.type) {
        case TASKS_ACTIONS.LOAD_TASKS:
            return {
                ...state,
                tasks: [...action.tasks]
            }

        case TASKS_ACTIONS.DELETE_TASK:
            return {
                ...state,
                tasks: [...state.tasks.filter(task => task.id !== action.taskId)]
            }
        case TASKS_ACTIONS.EDIT_TASK:
            const newTasks = state.tasks.filter(task => task.id !== action.task.id);
            newTasks.push(action.task)
            return {
                ...state,
                tasks: [...newTasks]
            }
        case TASKS_ACTIONS.ADD_TASK:
            state.tasks.push(action.task)
            return {
                ...state
            }

        default:
            return {
                ...state
            }
    }
}