import { Task } from 'src/app/models/task';

export interface TasksState {
    tasksData: {
        tasks: Task[]
    }
}