import { Action } from '@ngrx/store';
import { Task } from 'src/app/models/task';

export namespace TASKS_ACTIONS {
    export const ADD_TASK = 'ADD_TASK';
    export const DELETE_TASK = 'DELETE';
    export const EDIT_TASK = 'EDIT';
    export const LOAD_TASKS = 'LOAD_TASKS';
}

export class LoadTasks implements Action {

    readonly type = TASKS_ACTIONS.LOAD_TASKS;
    constructor(public tasks: Task[]) { }
}

export class AddTask implements Action {
    readonly type = TASKS_ACTIONS.ADD_TASK;
    constructor(public task: Task) { }
}

export class DeleteTask implements Action {
    readonly type = TASKS_ACTIONS.DELETE_TASK;
    constructor(public taskId: number) { }
}
export class EditTask implements Action {
    readonly type = TASKS_ACTIONS.EDIT_TASK;
    constructor(public task: Task) { }
}

export type TasksAction = AddTask | LoadTasks | DeleteTask | EditTask;