import { Component, OnInit, Input } from '@angular/core';
import { Task } from '../models/task';
import { Store } from '@ngrx/store';
import { TasksState } from '../redux/tasks/tasks.state';
import { DeleteTask } from '../redux/tasks/tasks.action';
import { DataService } from '../services/data.service';

@Component({
  selector: 'app-task-item',
  templateUrl: './task-item.component.html',
  styleUrls: ['./task-item.component.scss']
})
export class TaskItemComponent {

  public static taskEditableId: number;

  @Input() task: Task;
  @Input() isEditable: boolean;

  private _isEdit: boolean;

  constructor(
    private taskStore: Store<TasksState>,
    private dataService: DataService
  ) { }


  public get isEdit(): boolean {
    return this._isEdit && TaskItemComponent.taskEditableId === this.task.id;
  }

  public deleteItem(): void {
    this.dataService.deleteTask(this.task.id)
  }

  public enableEdit() {
    this._isEdit = true;
    TaskItemComponent.taskEditableId = this.task.id
  }


  public disableEdit() {
    this._isEdit = false;
  }

  private editItem(): void {
    this.dataService.editTask(this.task);
  }

  private addItem(): void {
    console.log(this.task)
    this.dataService.addTask(this.task);
  }

  public saveChanges() {
    this.editItem();
    this.disableEdit();
  }

}
