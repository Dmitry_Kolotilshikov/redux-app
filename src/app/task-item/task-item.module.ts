import { NgModule } from "@angular/core";
import { TaskItemComponent } from './task-item.component';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ThemeDirective } from '../themes/theme.directive';

@NgModule({
    declarations: [
        TaskItemComponent,
        ThemeDirective
    ],
    imports: [
        CommonModule,
        FormsModule
    ],
    exports: [
        TaskItemComponent
    ]
})

export class TaskItemModule { }