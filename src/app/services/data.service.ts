import { Injectable } from "@angular/core";
import { Task } from '../models/task';
import * as moment from 'moment';
import { Observable } from 'rxjs';
import { State, Store } from '@ngrx/store';
import { LoadTasks, DeleteTask, EditTask, AddTask } from '../redux/tasks/tasks.action';
import { TasksState } from '../redux/tasks/tasks.state';

@Injectable({ providedIn: 'root' })
export class DataService {

    private readonly tasks: Task[] = [
        {
            id: 0,
            text: 'Go to Shop',
            date: moment()
        },
        {
            id: 1,
            text: 'Sleep',
            date: moment()
        },
        {
            id: 2,
            text: 'Buy a car',
            date: moment()
        },
    ];

    constructor(private tasksStore: Store<TasksState>) { }

    loadTasks(): void {

        setTimeout(() => {
            this.tasksStore.dispatch(new LoadTasks(this.tasks))
        }, 2000)

    }

    deleteTask(id: number) {
        const task = this.tasks.find(t => t.id === id)
        const index = this.tasks.indexOf(task);
        this.tasks.splice(index, 1);
        this.tasksStore.dispatch(new DeleteTask(id));
    }

    editTask(task: Task): void {
        const editableTask = this.tasks.find(t => t.id === task.id)
        const index = this.tasks.indexOf(editableTask);
        this.tasks.splice(index, 1);
        this.tasks.push(task);
        this.tasksStore.dispatch(new EditTask(task))
    }

    addTask(task?: Task): void {
        if (!task) {
            task = new Task('Gomel');
        }

        const maxId = this.tasks.reduce((accum, item) => {
            return item.id > accum ? item.id : accum;
        }, 0)
        task.id = maxId + 1;

        this.tasks.push(task);
        this.tasksStore.dispatch(new AddTask(task))

    }
}