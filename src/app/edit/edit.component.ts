import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { TasksState } from '../redux/tasks/tasks.state';
import { Task } from '../models/task';
import { DataService } from '../services/data.service';


@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class EditComponent {

  newText = '';

  constructor(
    private tasksStore: Store<TasksState>,
    private dataService: DataService
  ) { }

  public get taskData() {
    return this.tasksStore.select('tasksData')
  }


  addTask(newText: string): void {
    // const newTask: Task = {
    //   id: null,
    //   text: 'New Task',
    //   date: moment()
    // };
    if (newText != '') {
      const newTask = new Task(newText);
      this.dataService.addTask(newTask)
      this.newText = '';
    }

  }
}
