import { NgModule } from "@angular/core";
import { EditComponent } from './edit.component';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { TaskItemModule } from '../task-item/task-item.module';
import { FormsModule } from '@angular/forms';

@NgModule({
    declarations: [
        EditComponent
    ],
    imports: [
        CommonModule,
        TaskItemModule,
        FormsModule,
        RouterModule.forChild([
            {
                path: '',
                component: EditComponent
            }
        ])
    ]
})

export class EditModule { }