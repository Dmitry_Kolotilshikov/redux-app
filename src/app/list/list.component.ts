import { Component, OnInit, OnDestroy } from '@angular/core';
import { DataService } from '../services/data.service';
import { Task } from '../models/task';
import { Subscription, Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { TasksState } from '../redux/tasks/tasks.state';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent {

  constructor(private tasksStore: Store<TasksState>) { }

  public get taskData() {
    return this.tasksStore.select('tasksData');
  }

}

