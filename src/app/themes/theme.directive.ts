import { Directive, OnInit, OnDestroy, ElementRef, Inject } from "@angular/core";
import { ThemeService } from './themes.service';
import { Subscription } from 'rxjs';
import { themes } from './themes';
import { DOCUMENT } from '@angular/common';

@Directive({
    selector: '[themeDirective]'
})

export class ThemeDirective implements OnInit, OnDestroy {

    private themeServiceSub: Subscription

    constructor(
        private themeService: ThemeService,
        private elementRef: ElementRef,
        @Inject(DOCUMENT) private document
    ) { }

    ngOnInit() {
        this.themeServiceSub = this.themeService.activeTheme.subscribe(theme => {
            this.updateTheme(theme)
        })
    }

    updateTheme(themeName: string): void {
        const theme = themes[themeName]
        const elem: HTMLElement = this.elementRef.nativeElement;
        for (const key in theme) {
            elem.style.setProperty(key, theme[key])
            this.document.body.style.setProperty(key, theme[key])
        }
    }

    ngOnDestroy() {
        if (this.themeServiceSub) {
            this.themeServiceSub.unsubscribe()
        }
    }

}