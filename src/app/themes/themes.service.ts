import { Injectable } from "@angular/core";
import { BehaviorSubject } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class ThemeService {

    public activeTheme = new BehaviorSubject('light');

    constructor() { }

    changeTheme(themeValue: string): void {
        this.activeTheme.next(themeValue)
    }

}
