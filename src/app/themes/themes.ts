export const themes = {
    light: {
        '--background': 'lightgray',
        '--text-color': 'black'
    },
    dark: {
        '--background': 'gray',
        '--text-color': 'white'
    }
};