import * as moment from 'moment';

export class Task {
    id: number;
    text: string;
    date: moment.Moment;

    constructor(text) {
        this.id = null;
        this.text = text,
            this.date = moment();
    }
}