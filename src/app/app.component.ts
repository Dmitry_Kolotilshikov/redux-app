import { Component, OnInit } from '@angular/core';
import { TasksState } from './redux/tasks/tasks.state';
import { Store } from '@ngrx/store';
import { LoadTasks } from './redux/tasks/tasks.action';
import { DataService } from './services/data.service';
import { ThemeService } from './themes/themes.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'redux-app';

  constructor(
    private dataService: DataService,
    private themeService: ThemeService
  ) { }

  ngOnInit() {
    this.dataService.loadTasks();
  }

  public toggleTheme(): void {
    const currentThemeName = this.themeService.activeTheme.getValue();
    const newThemeName = currentThemeName === 'light' ? 'dark' : 'light';
    this.themeService.changeTheme(newThemeName)
  }
}
